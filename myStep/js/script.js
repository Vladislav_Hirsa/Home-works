(function ($) {
    $(function () {

        $('ul.menu_section_third_block').on('click', 'li:not(.active)', function () {
            $(this)
                .addClass('active')
                .siblings()
                .removeClass('active')
                .closest('div.section_our_services')
                .find('div.section_third_about')
                .removeClass('active')
                .eq($(this).index())
                .addClass('active');
        });

    });
})(jQuery);

//Section Five

(function secFiveImg() {
    
    let chack = 0;
    let but_five = document.getElementById('but_five');
    let full_img = document.querySelectorAll("#full_img .img-box");
    let arrImg = Array.from(full_img);
    let gra_des = document.getElementById('gra-des');
    let web_des = document.getElementById('web-des');
    let land_p = document.getElementById('land-p');
    let w_ps = document.getElementById('w-ps');
    let all_img = document.getElementById('all-img');


    function showImg() {

        but_five.remove();

        for (let k in arrImg) {
            arrImg[k].classList.remove('img_y');
            arrImg[k].classList.add('img_n');
        }

        let ClassOfBut = this.classList[1];
        let y = document.getElementsByClassName(ClassOfBut);
        changeImg(y);
    }

    function changeImg(e) {
        Array.from(e, x => x.classList.add('img_y'));
    }


    gra_des.addEventListener('click', showImg);
    web_des.addEventListener('click', showImg);
    land_p.addEventListener('click', showImg);
    w_ps.addEventListener('click', showImg);
    all_img.addEventListener('click', showImg);

    but_five.addEventListener('click', function (event) {
        event.preventDefault();

        if (chack == 0) {
            chack = 1;
            for (var i = 12; i < 24; i++) {
                arrImg[i].classList.remove('img_n');
            }
        } else {
            event.preventDefault();
            for (i = 24; i < 36; i++) {
                arrImg[i].classList.remove('img_n');
                but_five.remove();
            }
        }
    });


}());

//Secton Six

(function secSixBlog() {
    let portf = document.querySelectorAll('.portfolio');
    let arrayPort = Array.from(portf)

    for (let i = 0; i < arrayPort.length; i++) {
        arrayPort[i].addEventListener('mouseover', mouseover);
        arrayPort[i].addEventListener('mouseout', mouseout);
    }

    function mouseover() {
        let blogOver = this.querySelector('.blog');
        blogOver.innerHTML = "Amazing Image Post";
    }

    function mouseout() {
        let blogOut = this.querySelector('.blog');
        blogOut.innerHTML = "Amazing Blog Post";
    }
}());



//Section Seven Karusel




(function slider(){
    
    let counter = 0;
let carouselBlocksTop = document.getElementsByClassName("workers_top")
let karusel_im = [...document.getElementsByClassName("one_of_workers")]
let carouselContainer = [...document.getElementsByClassName("carousel-item")];
let index = carouselContainer.findIndex((img) => img.classList.contains('active_'));
    
    function stepImg() {
    for (let i = 0; i < karusel_im.length; i++) {
        karusel_im[i].parentElement.classList.remove("active_")
    }

    karusel_im[index].parentElement.classList.add("active_")


    for (let i = 0; i < carouselBlocksTop.length; i++) {
        carouselBlocksTop[i].classList.remove("w_tf")
    }
    carouselBlocksTop[index].classList.add("w_tf")
}


document.getElementById("carousel-container").addEventListener("click", function (e) {
    if (e.target.classList.contains("one_of_workers")) {
        index = karusel_im.indexOf(e.target)
        stepImg()
    }
})


right_button.addEventListener('click', function () {
        ++index;
    
    if (index == karusel_im.length) {
        index = 0
    }
    
    stepImg()
});


left_button.addEventListener('click', function () {


    if (index == 0) {
        index = karusel_im.length
    }
    
        --index;
    stepImg()

});
    
}())

